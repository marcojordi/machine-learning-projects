# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import os
import traceback
import numpy as np
#pandas
import pandas as pd
from pandas.plotting import scatter_matrix
#Scikit-learn
from sklearn import svm
from sklearn import tree
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
#Utilities
from Utilities import PhaseDetector, SlicerResampler, ComputeStatValues
from Utilities import evaluate_model, compare_models, build_DNN, plot_loss
from Utilities import prep_targets_for_DNN, prep_targets_for_DNN_validate
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Wed Oct  3 11:28:20 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################

#Define global variables
SAMP_FREQ = 1000 
N_SLICES = 100
CLASSES = [1, 2, 3, 4, 5, 6, 7]

############################################################################### 

if __name__ == "__main__":

    try:
        
        #Load data and compute features
        path = "Database"
        labels = ["Time", "Acc"]
        files = os.listdir(path)
        
        features = pd.DataFrame()
        targets = pd.DataFrame()
        
        for ix in range(len(CLASSES)):
            data = pd.read_csv(path+"/"+files[ix], header=None)
            data.columns = labels
            
            #Apply PhaseDetector to shift the data
            time_shift, data_shift, zeropoints = PhaseDetector(data["Time"], data["Acc"], SAMP_FREQ, debug="no")

            #Apply SlicerResampler
            slices = SlicerResampler(data_shift, zeropoints, N_SLICES, debug="no")
    
            #Compute statistical values of slices for Input Data and labeling 
            feat = ComputeStatValues(slices)
            features = pd.concat((features, feat))
            
            #Create DataFrame with target Classes
            targ = pd.DataFrame(np.zeros((N_SLICES,1))+CLASSES[ix], columns=["Class"])
            targets = pd.concat((targets, targ))
        
        
        #######################################################################
        #Describe and visualize data
        all_data = features.copy()
        all_data["Targets"] = targets
        
        #Description
        all_data.describe()
        
        #Correlations
        corr_matrix = all_data.corr()
        print("\nFeature correlations to the target:")
        print(corr_matrix["Targets"].sort_values(ascending=False))
        
        #Histogramm
        #all_data.hist(bins=50)
        
        #Scatter Matrix
        scatter_matrix(all_data, alpha=0.2, diagonal='kde')

        
        #######################################################################
        #Prepare data for ML-Algorithmns
        
        # Split the data into a training set and a test set
        X_train, X_test, y_train, y_test = train_test_split(features, targets, random_state=0)
        
        #scaling input data
        scaler = StandardScaler()
        X_train_scale = scaler.fit_transform(X_train)
        X_test_scale = scaler.transform(X_test)
        
        #######################################################################
        #Test different ML-Algorithmns
        
        #SVM - Support Vector Machine
        
        svm_clf = svm.SVC()
        svm_clf.fit(X_train_scale, np.ravel(y_train))
        
        #SGD - Stochastic Gradient Descent Classifier
        sgd_clf = SGDClassifier(max_iter=1000, tol=0.1)
        sgd_clf.fit(X_train_scale, np.ravel(y_train))
        
        #Decision Tree
        tree_clf = tree.DecisionTreeClassifier()
        tree_clf.fit(X_train_scale, np.ravel(y_train))
        
        #K-Nearest Neighbor
        neigh_clf = KNeighborsClassifier()
        neigh_clf.fit(X_train_scale, np.ravel(y_train))
        
        #Random Forest
        rforest_clf = RandomForestClassifier(n_estimators=200)
        rforest_clf.fit(X_train_scale, np.ravel(y_train))
        
        #Deep Neural Network
        y_dnn_train = prep_targets_for_DNN(y_train, CLASSES)
        y_dnn_train_v = prep_targets_for_DNN_validate(y_train)
        y_dnn_test_v = prep_targets_for_DNN_validate(y_test)
        
        dnn_clf = build_DNN()
        hist = dnn_clf.fit(X_train_scale, y_dnn_train, epochs=1000)
        plot_loss(hist)
  
        
        #######################################################################
        #Compare models
        
        models = [svm_clf, sgd_clf, tree_clf, neigh_clf, rforest_clf, dnn_clf]
        model_names = ["SVM", "SGD", "Decision tree", "KNN", "Random Forest", "NN"]
        class_names = ["1", "2", "3", "4", "5", "6", "7"]
        
        print("\nEvaluate SVM:")
        evaluate_model(svm_clf, X_test_scale, y_test, X_train_scale, y_train)
        
        print("\nEvaluate SGD:")
        evaluate_model(sgd_clf, X_test_scale, y_test, X_train_scale, y_train)
        
        print("\nEvaluate Decision Tree:")
        evaluate_model(tree_clf, X_test_scale, y_test, X_train_scale, y_train)
        
        print("\nEvaluate K-Nearest Neighbor:")
        evaluate_model(neigh_clf, X_test_scale, y_test, X_train_scale, y_train)
        
        print("\nEvaluate Random Forest:")
        evaluate_model(rforest_clf, X_test_scale, y_test, X_train_scale, y_train)
        
        print("\nEvaluate NN:")
        evaluate_model(dnn_clf, X_test_scale, y_dnn_test_v, X_train_scale, y_dnn_train_v, keras="yes")
        
        compare_models(models, model_names, class_names, X_test_scale, y_test, y_dnn_test_v)
        
        
    except:
        traceback.print_exc()
