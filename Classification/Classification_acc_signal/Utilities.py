# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import math
import itertools
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal, stats
#scikit-learn
from sklearn.metrics import accuracy_score, confusion_matrix
#keras
from keras.models import Sequential
from keras.layers import Dense,Dropout
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Wed Oct  3 12:36:14 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################
def PhaseDetector(time, data, fs, debug="no"):
    """Shift the data to the first zeropoint
    
    Args:
        :time: Array or Series with time data
        :data: Array or Series with acceleration data
        :fs: Int with smpling frequency
        :debug: Flag to plot debug diagramms
        
    Return:
        :t_shift: Array or Series with shifted time data
        :data_shift: Array or Series with shifted acceleration data
        :zeropoints_index_shift: List of zeropoints
    """ 
    #Detrending acceleration signal
    data = signal.detrend(data)
    
    #Filtering acceleration signal
    b, a = signal.iirfilter(4, Wn=0.01, btype='lowpass', ftype='butter')
    filt_data = signal.filtfilt(b, a, data, axis=0)
    
    #Finding zero points of filtered signal
    zeropoints_time = []
    zeropoints_index = []
    for i in np.arange(len(filt_data)-1):
        if filt_data[i] > 0 and filt_data[i+1] < 0:
            zeropoints_index.append(i)
            zeropoints_time.append(time[i])
    
    #Shift signal to first zero point
    t_shift = time[zeropoints_index[0]:-1]
    data_shift = data[zeropoints_index[0]:-1]
    zeropoints_index_shift = zeropoints_index - zeropoints_index[0]
    
    if debug == "yes":    
        #plotting original data 
        plt.figure()
        plt.plot(time,data)
        plt.grid()
        #plotting filtered data with zero points
        plt.figure()
        plt.plot(time,filt_data)
        plt.grid()
        plt.scatter(zeropoints_time, np.zeros(len(zeropoints_time)), color="red")
        #plotting shifted data
        plt.figure()
        plt.plot(t_shift,data_shift)
        plt.grid()
        
    return t_shift, data_shift, zeropoints_index_shift


def SlicerResampler(data, zeropoints, n_slices=10, debug="no"):
    """Slice the data in slices of one turn and resample it
    
    Args:
        :data: Array of shifted data
        :zeropoints: Array of zeropoints
        :n_slices: Int with number of slices
        :debug: Flag to plot debug diagramms
        
    Return:
        :slices: Array with acceleration Data of slices
    """
    #Init nomber of resampling samples and slice array
    n_samples_resampled = 2**10 
    slices = np.zeros([n_slices,n_samples_resampled])
    
    for ix in np.arange(n_slices):
         sliced_data = data[zeropoints[ix]:zeropoints[ix+1]]
         if len(sliced_data) < n_samples_resampled:
             del sliced_data
             print("Deleted slice nr.:{}".format(ix))
         else:
             sliced_res_data = signal.resample(sliced_data,n_samples_resampled)
             slices[ix] = np.transpose(sliced_res_data)
    
    if debug == "yes":
        #Plots for debugging
        plt.figure()
        plt.plot(np.transpose(slices))
        plt.grid()
    
    return slices


def ComputeStatValues(slices):
    """ Computes the statistical values of the slices
    
    Args:
        :slices: Array with slices with acceleration data from one turn
           
    Return:
        :slices_stats: DataFrame with statistical values of slices
    """
    
    sl_rms = np.sqrt(np.mean(slices**2,axis=1))
    sl_max = np.max(slices,axis=1)
    sl_min = np.min(slices,axis=1)
    sl_std = np.std(slices,axis=1)
    sl_kurt = stats.kurtosis(slices,axis=1)
    sl_skew = stats.skew(slices,axis=1)
    
    names = ["sl_rms","sl_max","sl_min","sl_std","sl_kurt","sl_skew"] 
    
    slices_stats = pd.DataFrame(np.transpose([sl_rms, sl_max, sl_min, sl_std, 
                                              sl_kurt, sl_skew]), columns=names)
    
    return slices_stats

def evaluate_model(model, X_test, y_test, X_train=None, y_train=None, prnt="yes", keras="no"):
    """ Evaluate the accuracy and F1-score of the model
    
    Args:
        :model: Trained machine learning model to predict data
        :X_test: Test data inputs
        :y_test: Test data targets
        :X_train: Training data inputs
        :y_train: Training data targets
        
    Return:
        :acc: Accuracy of the model
        :f1: F1-score of the model
        :confmat: Confiusion matrix
    """
    #Calculates predictions
    if keras == "yes":
        prediction_test = model.predict_classes(X_test)
        if type(X_train) != type(None):
            prediction_train = model.predict_classes(X_train)
        
    else:
        prediction_test = model.predict(X_test)
        if type(X_train) != type(None):
            prediction_train = model.predict(X_train)
        
    
    #Calculate accuracy and confusion matrix
    if type(X_train) != type(None):
        acc_train = accuracy_score(y_train, prediction_train)
        
    acc_test = accuracy_score(y_test, prediction_test)
    confmat = confusion_matrix(y_test, prediction_test)
    
    if prnt =="yes":
        print("  Training Accuracy is: ", acc_train) 
        print("  Test Accuracy is:     ", acc_test) 
    
    return acc_test, confmat
    

def compare_models(models, labels, classes, X, y, y_dnn):
    """ Compare different machine learning models
    
    Args:
        :models: List of trained machine learning models to predict data
        :labels: List with names of models
        :classes: List with names of classes
        :X: Test data inputs
        :y: Test data targets
        
    """
    n = 0
    fig = plt.figure(figsize=(12,8))
    
    for model in models:
        
        #Evaluate model
        try:
            acc, confmat = evaluate_model(model, X, y, prnt="no", keras="no")
        except:
            acc, confmat = evaluate_model(model, X, y_dnn, prnt="no", keras="yes")
        
        #Normalize confusion Matrix
        cm = (confmat.astype('float') / confmat.sum(axis=1)[:, np.newaxis])*100
        
        ax = fig.add_subplot(math.ceil(len(models)/3),3,n+1)
        ax.set_title(labels[n]+", Acc: "+str("%.2f" %acc))
        im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
        
        #Set ticks
        tick_marks = np.arange(len(classes))
        ax.set_xticks(tick_marks)
        ax.set_yticks(tick_marks)
        ax.set_xticklabels(classes, fontsize=6)
        ax.set_yticklabels(classes, fontsize=6)
        
        #Set axes labels
        if (n+1)%3 == 1:
            ax.set_ylabel('True class')
        ax.set_xlabel('Predicted class')
    
        #Plot text with corret or false classified inputs in percent
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            ax.text(j, i, format(cm[i, j], '.0f')+"%",
                    horizontalalignment="center",
                    color="white" if cm[i, j] > thresh else "black", size=6)
        n+=1
    
    #Add Colorbar to plot
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    
    fig.suptitle("Accuracy and Confusion Matrix of Testdataset",fontsize=15)
    

def build_DNN():
    """ Build a Deep Neural Network
    
    Args:
           
    Return:
        :model: Keras Model of DNN
    """
    #Define model
    model = Sequential()
    
    #input layer
    model.add(Dense(units=6, input_dim=6, activation="linear"))
    
    #hidden layers 
    model.add(Dense(units=80, activation="selu"))
    model.add(Dropout(0.5))
    model.add(Dense(units=80, activation="selu"))
    model.add(Dropout(0.5))
    model.add(Dense(units=80, activation="selu"))
    model.add(Dense(units=80, activation="selu"))
    model.add(Dense(units=40, activation="selu"))
    
    #output layer
    model.add(Dense(units=7, activation='softmax'))
    
    #compile model
    model.compile(loss='categorical_crossentropy', optimizer='Nadam')
    model.summary()
    
    return model

def prep_targets_for_DNN(y, classes):
    """ Preperation of targets for keras DNN
    
    Args:
        :y: Array with targets
        :classes: Array with Classes
           
    Return:
        :y_dnn: Array with classes binary
    """
    y_dnn = np.zeros((len(y),len(classes)))
     
    for i in range(len(y)):
        if y.iloc[i].values == 1:
            y_dnn[i][0] = 1
        elif y.iloc[i].values == 2:
            y_dnn[i][1] = 1
        elif y.iloc[i].values == 3:
            y_dnn[i][2] = 1
        elif y.iloc[i].values == 4:
            y_dnn[i][3] = 1
        elif y.iloc[i].values == 5:
            y_dnn[i][4] = 1
        elif y.iloc[i].values == 6:
            y_dnn[i][5] = 1
        else:
            y_dnn[i][6] = 1
    
    return y_dnn

def prep_targets_for_DNN_validate(y):
    """ Preperation of targets for keras DNN to validate
    
    Args:
        :y: Array with targets
           
    Return:
        :y_dnn: Array with classes
    """
    y_dnn = np.zeros((len(y),1))
    
    for i in range(len(y)):
        if y.iloc[i].values == 1:
            y_dnn[i] = 0
        elif y.iloc[i].values == 2:
            y_dnn[i] = 1
        elif y.iloc[i].values == 3:
            y_dnn[i] = 2
        elif y.iloc[i].values == 4:
            y_dnn[i] = 3
        elif y.iloc[i].values == 5:
            y_dnn[i] = 4
        elif y.iloc[i].values == 6:
            y_dnn[i] = 5
        else:
            y_dnn[i] = 6
            
    return y_dnn
    

def plot_loss(hist):
    """Plotting loss diagram of DNN learning 
    
    Args:
        :hist: learning history of keras model
    """
    
    history_dict = hist.history
    loss_values = history_dict['loss']
    
    plt.figure()
    plt.plot(loss_values)
    plt.title("DNN loss during training")
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.grid()
    
    
    
    
    
    
    
    
    