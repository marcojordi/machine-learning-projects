# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import traceback
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use("../../plot_style.mplstyle")
#scikit-learn
from sklearn import svm
from sklearn.datasets import load_iris
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Tue Oct  2 15:59:42 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################
   
if __name__ == "__main__":

    try:
        
        #Load iris data to a pandas Dataframe
        iris = load_iris()
        df = pd.DataFrame(iris.data, columns=iris.feature_names)
        
        #Describe Data
        print("Description:\n", df.describe())
        
        #Print Head (top five rows)
        print("Head:\n", df.head())
        
        #Plot Histogram
        df.hist(bins=30)
        
        # Split the data into a training set and a test set
        X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, random_state=0)

        #Visualizing train data
        iris.feature_names.append("Target class")
        train_data = pd.DataFrame(np.hstack((X_train, y_train.reshape(len(y_train),1))), columns=iris.feature_names )
        
        #Scatter Plot
        train_data.plot(kind="scatter", x="sepal length (cm)", y="petal length (cm)", c="Target class", colormap='viridis')
        
        #looking for correlations and plot it
        corr_matrix = train_data.corr()
        print(corr_matrix["Target class"].sort_values(ascending=False))
        
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.matshow(corr_matrix, vmin=-1, vmax=1, cmap="jet")
        fig.colorbar(cax)
        ax.set_xticks(np.arange(0,len(iris.feature_names),1))
        ax.set_yticks(np.arange(0,len(iris.feature_names),1))
        ax.set_xticklabels(iris.feature_names, rotation=15)
        ax.set_yticklabels(iris.feature_names, rotation=30)
        ax.tick_params(labelsize=8)
        
        #Droping sepal width
        X_train = np.delete(X_train, obj=1, axis=1)
        X_test = np.delete(X_test, obj=1, axis=1)
        
        #scaling input data
        scaler = StandardScaler()
        X_train_scale = scaler.fit_transform(X_train)
        X_test_scale = scaler.transform(X_test)
        
        #train SVM
        model = svm.SVC()
        model.fit(X_train_scale, y_train)  
        
        #verify model
        prediction_train = model.predict(X_train_scale)
        prediction_test = model.predict(X_test_scale)
        f1_train = accuracy_score(y_train, prediction_train)
        f1_test = accuracy_score(y_test, prediction_test)
        
        print("\nTraining Accuracy is: ", f1_train)
        print("Test Accuracy is:     ", f1_test)
        
        cnf = confusion_matrix(y_test, prediction_test)
        print("\nConfusion Matrix of Test Dataset:")
        print(cnf)
        
        
    except:
        traceback.print_exc()
