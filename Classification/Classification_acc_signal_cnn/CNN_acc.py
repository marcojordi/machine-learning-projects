# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import os
import traceback
import numpy as np
import pandas as pd
#scikit-learn
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
#utilities
from Utilities import PhaseDetector, SlicerResampler, prep_targets_for_DNN
from Utilities import evaluate_model, plot_loss, plot_confmat, build_cnn2, callbacks
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Wed Oct  3 11:28:20 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################

#Define global variables
SAMP_FREQ = 1000 
N_SLICES = 110
CLASSES = [1, 2, 3, 4, 5, 6, 7]

############################################################################### 

if __name__ == "__main__":

    try:
        
        #Load data and compute slices
        path = "../Classification_acc_signal/Database/"
        labels = ["Time", "Acc"]
        files = os.listdir(path)
        
        slices = pd.DataFrame()
        targets = pd.DataFrame()
        
        for ix in range(len(CLASSES)):
            data = pd.read_csv(path+"/"+files[ix], header=None)
            data.columns = labels
            
            #Apply PhaseDetector to shift the data
            time_shift, data_shift, zeropoints = PhaseDetector(data["Time"], data["Acc"], SAMP_FREQ, debug="no")

            #Apply SlicerResampler
            slic = SlicerResampler(data_shift, zeropoints, N_SLICES, debug="no")
            slic = pd.DataFrame(slic)
            slices = pd.concat((slices, slic))
            
            #Create DataFrame with target Classes
            targ = pd.DataFrame(np.zeros((N_SLICES,1))+CLASSES[ix], columns=["Class"])
            targets = pd.concat((targets, targ))
        
        targets = prep_targets_for_DNN(targets, CLASSES)
        
        
        #######################################################################
        #Prepare data for ML-Algorithmns
        
        # Split the data into a training set and a test set
        X_train, X_test, y_train, y_test = train_test_split(slices, targets, random_state=0)
        
        #scaling input data and reshape it
        scaler = StandardScaler()
        X_train_scale = scaler.fit_transform(X_train)
        X_test_scale = scaler.transform(X_test)
        
        X_train_scale = np.array(X_train_scale).reshape(len(X_train), len(X_train.iloc[0]), 1)
        X_test_scale = np.array(X_test_scale).reshape(len(X_test), len(X_test.iloc[0]), 1)
        
        #Prepare targets for evaluation
        y_train_val = np.argmax(y_train, axis=1)
        y_test_val = np.argmax(y_test, axis=1)
        
        #######################################################################
        #Create and train CNN
        batch_size = 25
        epochs = 100
        input_shape = (len(X_train.iloc[0]), 1)
        n_classes = len(CLASSES)
        kernel_size = 3
        
        #Build CNN
        model = build_cnn2(input_shape, n_classes, kernel_size)
        
        #Define callback informations for tensorboard, early stopping and 
        #saving best model
        clbks = callbacks()

        #Train model and memorize training history
        hist = model.fit(X_train_scale, y_train,
                         batch_size=batch_size,
                         epochs=epochs,
                         callbacks=clbks,
                         validation_split=0.15)
        
        #Load best weights for model
        model.load_weights("model/best.h5")
        model.compile(loss='categorical_crossentropy',
                  optimizer='nadam', metrics=['accuracy'])
        
        #Evaluate model
        evaluate_model(model, X_test_scale, y_test_val, X_train_scale, y_train_val, prnt="yes", keras="yes")
        plot_confmat(model, "CNN", CLASSES, X_test_scale, y_test_val)
        plot_loss(hist)
        
    except:
        traceback.print_exc()
