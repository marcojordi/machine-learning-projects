# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import itertools
import datetime
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
#keras
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv1D, MaxPooling1D, GlobalAveragePooling1D
from keras.optimizers import SGD
#scikit-learn
from sklearn.metrics import accuracy_score, confusion_matrix
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Wed Oct  3 12:36:14 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################
def PhaseDetector(time, data, fs, debug="no"):
    """Shift the data to the first zeropoint

    Args:
        :time: Array or Series with time data
        :data: Array or Series with acceleration data
        :fs: Int with smpling frequency
        :debug: Flag to plot debug diagramms

    Return:
        :t_shift: Array or Series with shifted time data
        :data_shift: Array or Series with shifted acceleration data
        :zeropoints_index_shift: List of zeropoints
    """
    #Detrending acceleration signal
    data = signal.detrend(data)

    #Filtering acceleration signal
    b, a = signal.iirfilter(4, Wn=0.01, btype='lowpass', ftype='butter')
    filt_data = signal.filtfilt(b, a, data, axis=0)

    #Finding zero points of filtered signal
    zeropoints_time = []
    zeropoints_index = []
    for i in np.arange(len(filt_data)-1):
        if filt_data[i] > 0 and filt_data[i+1] < 0:
            zeropoints_index.append(i)
            zeropoints_time.append(time[i])

    #Shift signal to first zero point
    t_shift = time[zeropoints_index[0]:-1]
    data_shift = data[zeropoints_index[0]:-1]
    zeropoints_index_shift = zeropoints_index - zeropoints_index[0]

    if debug == "yes":
        #plotting original data
        plt.figure()
        plt.plot(time,data)
        plt.grid()
        #plotting filtered data with zero points
        plt.figure()
        plt.plot(time,filt_data)
        plt.grid()
        plt.scatter(zeropoints_time, np.zeros(len(zeropoints_time)), color="red")
        #plotting shifted data
        plt.figure()
        plt.plot(t_shift,data_shift)
        plt.grid()

    return t_shift, data_shift, zeropoints_index_shift


def SlicerResampler(data, zeropoints, n_slices=10, debug="no"):
    """Slice the data in slices of one turn and resample it

    Args:
        :data: Array of shifted data
        :zeropoints: Array of zeropoints
        :n_slices: Int with number of slices
        :debug: Flag to plot debug diagramms

    Return:
        :slices: Array with acceleration Data of slices
    """
    #Init nomber of resampling samples and slice array
    n_samples_resampled = 2**10
    slices = np.zeros([n_slices,n_samples_resampled])

    for ix in np.arange(n_slices):
         sliced_data = data[zeropoints[ix]:zeropoints[ix+1]]
         if len(sliced_data) < n_samples_resampled:
             del sliced_data
             print("Deleted slice nr.:{}".format(ix))
         else:
             sliced_res_data = signal.resample(sliced_data,n_samples_resampled)
             slices[ix] = np.transpose(sliced_res_data)

    if debug == "yes":
        #Plots for debugging
        plt.figure()
        plt.plot(np.transpose(slices))
        plt.grid()

    return slices


def evaluate_model(model, X_test, y_test, X_train=None, y_train=None, prnt="yes", keras="no"):
    """ Evaluate the accuracy and F1-score of the model

    Args:
        :model: Trained machine learning model to predict data
        :X_test: Test data inputs
        :y_test: Test data targets
        :X_train: Training data inputs
        :y_train: Training data targets

    Return:
        :acc: Accuracy of the model
        :f1: F1-score of the model
        :confmat: Confiusion matrix
    """
    #Calculates predictions
    if keras == "yes":
        prediction_test = model.predict_classes(X_test)
        if type(X_train) != type(None):
            prediction_train = model.predict_classes(X_train)

    else:
        prediction_test = model.predict(X_test)
        if type(X_train) != type(None):
            prediction_train = model.predict(X_train)


    #Calculate accuracy and confusion matrix
    if type(X_train) != type(None):
        acc_train = accuracy_score(y_train, prediction_train)

    acc_test = accuracy_score(y_test, prediction_test)
    confmat = confusion_matrix(y_test, prediction_test)

    if prnt == "yes":
        print("  Training Accuracy is: ", acc_train)
        print("  Test Accuracy is:     ", acc_test)

    return acc_test, confmat


def plot_confmat(model, label, classes, X, y):
    """ Compare different machine learning models

    Args:
        :model: trained machine learning model to predict data
        :label: List with names of models
        :classes: List with names of classes
        :X: Test data inputs
        :y: Test data targets

    """
    fig = plt.figure(figsize=(12,8))

    #Evaluate model
    acc, confmat = evaluate_model(model, X, y, prnt="no", keras="yes")

    #Normalize confusion Matrix
    cm = (confmat.astype('float') / confmat.sum(axis=1)[:, np.newaxis])*100

    ax = fig.add_subplot(1,1,1)
    ax.set_title(label+", Acc: "+str("%.2f" %acc))
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)

    #Set ticks
    tick_marks = np.arange(len(classes))
    ax.set_xticks(tick_marks)
    ax.set_yticks(tick_marks)
    ax.set_xticklabels(classes, rotation=30, fontsize=6)
    ax.set_yticklabels(classes, fontsize=6)

    #Set axes labels
    ax.set_ylabel('True class')
    ax.set_xlabel('Predicted class')

    #Plot text with corret or false classified inputs in percent
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        ax.text(j, i, format(cm[i, j], '.0f')+"%",
                horizontalalignment="center",
                color="white" if cm[i, j] > thresh else "black", size=6)


    #Add Colorbar to plot
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)

    fig.suptitle("Accuracy and Confusion Matrix of Testdataset",fontsize=15)


def prep_targets_for_DNN(y, classes):
    """ Preperation of targets for keras DNN

    Args:
        :y: Array with targets
        :classes: Array with Classes

    Return:
        :y_dnn: Array with classes binary
    """
    y_dnn = np.zeros((len(y),len(classes)))

    for i in range(len(y)):
        if y.iloc[i].values == 1:
            y_dnn[i][0] = 1
        elif y.iloc[i].values == 2:
            y_dnn[i][1] = 1
        elif y.iloc[i].values == 3:
            y_dnn[i][2] = 1
        elif y.iloc[i].values == 4:
            y_dnn[i][3] = 1
        elif y.iloc[i].values == 5:
            y_dnn[i][4] = 1
        elif y.iloc[i].values == 6:
            y_dnn[i][5] = 1
        else:
            y_dnn[i][6] = 1

    return y_dnn


def plot_loss(hist):
    """Plotting loss diagram of DNN learning

    Args:
        :hist: learning history of keras model
    """

    history_dict = hist.history
    loss_values = history_dict['loss']

    plt.figure()
    plt.plot(loss_values)
    plt.title("CNN loss during training")
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.grid()


def build_cnn1(input_shape, n_classes, kernel_size):
    """Build and compile keras cnn model (VGG-like convnet)

    """
    #Initialize Model
    model = Sequential()

    #Input Convolution layer
    model.add(Conv1D(32, kernel_size, activation='relu', input_shape=input_shape))

    #Hidden layers
    model.add(Conv1D(32, kernel_size, activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Dropout(0.25))

    model.add(Conv1D(64, kernel_size, activation='relu'))
    model.add(Conv1D(64, kernel_size, activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    #Output layer
    model.add(Dense(n_classes, activation='softmax'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy',
                  optimizer=sgd, #"adam"
                  metrics=['accuracy'])
    model.summary()

    return model

def build_cnn2(input_shape, n_classes, kernel_size):
    """Build and compile keras cnn model

    """
    #Initialize Model
    model = Sequential()

    #Input Convolution layer
    model.add(Conv1D(64, kernel_size, activation='relu', input_shape=input_shape))

    #Hidden layers (convolution and Pooling layers)
    model.add(Conv1D(64, kernel_size, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Conv1D(128, kernel_size, activation='relu'))
    model.add(Conv1D(128, kernel_size, activation='relu'))
    model.add(GlobalAveragePooling1D())
    model.add(Dropout(0.5))

    #Output layer (fully conected dense layer)
    model.add(Dense(n_classes, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='nadam', #"adam"
                  metrics=['accuracy'])
    model.summary()

    return model

def build_cnn3(input_shape, n_classes, kernel_size):
    """Build and compile keras cnn model

    """
    #Initialize Model
    model = Sequential()

    #Input Convolution layer
    model.add(Conv1D(64, kernel_size, activation='relu', input_shape=input_shape))

    #Hidden layers (convolution and Pooling layers)
    model.add(Conv1D(64, kernel_size, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Conv1D(128, kernel_size, activation='relu'))
    model.add(Conv1D(128, kernel_size, activation='relu'))
    model.add(MaxPooling1D(3))
    model.add(Dropout(0.5))
    model.add(Conv1D(256, kernel_size, activation='relu'))
    model.add(Conv1D(256, kernel_size, activation='relu'))
    model.add(GlobalAveragePooling1D())
    model.add(Dropout(0.5))


    #Output layer (fully conected dense layer)
    model.add(Dense(n_classes, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='nadam', #"adam"
                  metrics=['accuracy'])
    model.summary()

    return model

def callbacks():
    """Define callbacks for keras model fit
    
    """
    #Init list of callbacks
    clbks = []
    name = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    
    #Define informations required for tensorboard, run Tensorboard 
    #in AnacondapPrompt with: tensorboard --logdir="/path_to_your_logs"
    tensorboard = keras.callbacks.TensorBoard(log_dir="tensorboard/"+name+"/",
                                              write_graph=True, histogram_freq=1)
    clbks.append(tensorboard)
    
    #Define informations required for early stopping
    earlystop = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, 
                                              patience=20, mode='auto')
    clbks.append(earlystop)
    
    #Define informations required for saving best model
    save_best = keras.callbacks.ModelCheckpoint(filepath="model/best.h5", 
                                                save_best_only=True)
    clbks.append(save_best)
    
    return clbks


