# Projects with Classification tasks
This directory contains various Machine Learning Projects with classification tasks, written in Python 3.6.xx using the Machine Learning libraries [Scikit-Learn](https://github.com/scikit-learn/scikit-learn) and [Keras](https://github.com/keras-team/keras) (backend [TensorFlow](https://github.com/tensorflow/tensorflow)).

- Classification_acc_signal: 

  Classification of seven different acceleration signals with six different Machine Learning Algorithms (SVM, SGD, Decision tree, KNN, Random Forest, Feedforward Neural Network)

- Classification_acc_signal_cnn: 

  Classification of seven different acceleration signals with a 1D Convolutional Neural Network
  
- Classification_iris_svm:

  Classification of iris data with a Support Vector machine