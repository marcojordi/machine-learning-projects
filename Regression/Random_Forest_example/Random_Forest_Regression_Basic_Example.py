# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import traceback
import numpy as np
#scikit-learn
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
#Utilities
from Utilities import load_data, preprocess_train_data
from Utilities import describe_visual, evaluate_model
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Fri Oct  5 15:51:34 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################
    
if __name__ == "__main__":

    try:
        
        #Load data
        path = "Database"
        labels = ["X1","X2","X3"]
        input_data, target_data = load_data(path, labels)
        
        #Preprocess data
        inputs, targets = preprocess_train_data(input_data, target_data)
        
        #Describe and visualize data
        describe_visual(inputs, targets)
        
        # Split the data into a training set and a test set
        X_train, X_test, y_train, y_test = train_test_split(inputs, targets, random_state=0)
        
        #scaling input data
        scaler = StandardScaler()
        X_train_scale = scaler.fit_transform(X_train)
        X_test_scale = scaler.transform(X_test)
        
        #Random Forest
        print("5. Train Random Forest Algorithm (Training dataset)")
        forest_reg = RandomForestRegressor(n_estimators=100, random_state=42)
        forest_reg.fit(X_train_scale, np.ravel(y_train))
        predictions_forest = forest_reg.predict(X_train_scale)
        evaluate_model(y_train, predictions_forest)
        
        #Make Predictions on test set and evaluate model
        print("6. Test and evaluate Random Forest Algorithm (Test dataset)")
        predictions_forest = forest_reg.predict(X_test)
        evaluate_model(y_test, predictions_forest)
        
    except:
        traceback.print_exc()
