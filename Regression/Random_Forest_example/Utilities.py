# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use("../../plot_style.mplstyle")
from sklearn.metrics import mean_squared_error
from scipy.signal import butter, lfilter, detrend
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Fri Oct 26 12:10:51 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################

def load_data(path, labels):
    """
    Loading Data from Database path
    
    Args:
        path: String with Database path \n
        labels:List with labels
    Return:
        input_data: Dataframe with loaded inputs \n
        target_data: Dataframe with loaded targets
    """
    
    print("1. Load Data")
    
    #Input data
    x1 = np.load(path+"/X1.npy")
    x2 = np.load(path+"/X2.npy")
    x3 = np.load(path+"/X3.npy")
    
    input_data = pd.DataFrame(np.array((x1, x2, x3)).transpose(), 
                              columns=labels)
    #Target data
    y = np.load(path+"/y.npy")
    
    target_data = pd.DataFrame(y, columns=["y"])
    
    return input_data, target_data


def preprocess_train_data(input_data, target_data):
    """
    Preprocessing the data for trainig the ml-algorithm
    
    Args:
        
    Return:
        inputs_red: Dataframe with inputs (reduced features) \n
        targets_red: Dataframe with targets (reduced targets)
    """
    
    #Feature extraction
    print("2. Preprocessing Data")
    input_data, target_data = feature_extract(input_data, target_data)
         
    #Feature reduction
    print("3. Reducing Features")
    inputs_red, targets_red = reduce_FeatureSamples(input_data, target_data)
        
    return inputs_red, targets_red

def feature_extract(input_data, target_data):
    """
    Extract features like moving mean and std from 1000 Hz data 
    
    Args:
        
    Return:
        features: Dataframe with features \n
        sum_y_2m: Series with sum y 2m average value
    """
    x1 = np.array(input_data["X1"])
    x2 = np.array(input_data["X2"])
    x3 = np.array(input_data["X3"])

    
    y = np.array(target_data["y"])
    
    #Filtering x1 signal
    print("  - Filtering")
    x1_f = butter_lowpass_filter(data=x1, cutoff=5, fs=1000, order=5)
    
    #Detrending x1 and x2 Signals
    print("  - Detrending")
    x1_fd = detrend(x1_f, axis=0)
    x2_fd = detrend(x2, axis=0)
    
    #Calculate moving windows and trimm all datasets on same length
    print("  - Calculate moving windows")
    x1_window = create_dataset(x1_fd, moving_window=100)
    x2_window = create_dataset(x2_fd, moving_window=50)
    x3_window = create_dataset(x3, moving_window=50)

    x2_window = x2_window[25:-25]
    x3_window = x3_window[25:-25]

    #Calculate Features
    print("  - Calculate features")
    x1_mean, x1_std = calc_features(x1_window) 
    x2_mean, x2_std = calc_features(x2_window) 
    x3_mean, x3_std = calc_features(x3_window) 
    
    features = [x1_mean, x1_std, x2_mean, x2_std, x3_mean, x3_std]
    features = np.transpose(np.vstack(features))
    features = pd.DataFrame(features, columns=["x1_mean", "x1_std", "x2_mean",
                                               "x2_std", "x3_mean", "x3_std"])
    
    #Target data
    print("  - Prepare target data")
    y = y[50:-51]
    target = pd.DataFrame(y, columns=["y"])
    
    return features, target

def butter_lowpass_filter(data, cutoff, fs, order=5):
    """
    Apply butterworth filter to data
    
    Args:
        data: Dataframe with sensordata \n
        cutoff: Integer with cutoff frequency \n
        fs: Integer with samplingrate \n
        order: Integer with order of the butterworthfilter
    
    Return:
        y: lowpass filtered data
    """
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = lfilter(b, a, data, axis=0)
    return y

def create_dataset(dataset, moving_window=20):
    """
    Convert an array of values into a dataset matrix
    
    Args:
        dataset: Dataframe with sensordata \n
        moving_window: Integer of window size
        
    Return:
        sum_y_2m: Series with sum y 2m average value
    """
    dataX = []
    for i in range(len(dataset)-moving_window-1):
        a = dataset[i:(i+moving_window)]
        dataX.append(a)
    return np.array(dataX)

def calc_features(data):
    """
    Calculates the the features mean, std, max, min, kurt, skew for data with moving window
    
    Args:
        data: Dataframe with sensordata
        
    Return:
        d_mean: Mean value of data in axis 1 \n
        d_std: Standard deviation of data in axis 1
    """
    d_mean = np.mean(data, axis=1)
    d_std = np.std(data, axis=1)

    return d_mean, d_std

def reduce_FeatureSamples(features, target):
    """
    Reducing the number of samples in the input data set to 5000 values in 
    one class
    
    Args:
        features: Dataframe with the features \n
        target: Dataframe with the targets \n
        
    Return:
        features_red: Dataframe with reduced features \n
        sumy2m_red: Dataframe with reduced targets
    """
    
    classes = np.arange(-50,50,0.5)
    y_red = pd.DataFrame()

    for ix in range(len(classes)-2):
        temp = target.y[(target.y > classes[ix]) & (target.y < classes[ix+1])]
        if len(temp) > 0 and len(temp) < 100:
            y_red = pd.concat([y_red, temp])
        elif len(temp) > 200:
            y_red = pd.concat([y_red, temp.sample(100)])
        else:
            pass
  
    features_red = features.iloc[y_red.index]

    return features_red, y_red


def describe_visual(inputs, targets):
    """Describe and visualize input and target data
    
    Args:
        inputs: Dataframe with the inputs \n
        targets: Dataframe with the targets \n
        
    Return:
    """
    print("4. Describe and Visualize Data")
    inputs.hist(bins=100)
    #targets.hist(bins=200)
    
    all_data = inputs.copy()
    all_data["Targets"] = targets
    
    print("  - Feature correlations to the target")
    corr_matrix = all_data.corr()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(corr_matrix, vmin=-1, vmax=1, cmap="jet")
    fig.colorbar(cax)
    ax.set_xticks(np.arange(0,len(all_data.columns),1))
    ax.set_yticks(np.arange(0,len(all_data.columns),1))
    ax.set_xticklabels(all_data.columns, rotation=30)
    ax.set_yticklabels(all_data.columns, rotation=30)
    ax.tick_params(labelsize=8)
    
def evaluate_model(target, prediction):
    """
    Evaluate ml-model and calculate RMSE and correlation coefficient
    
    Args:
        :target: Array with target data
        :prediction: Array with predicted data
        
    Return:
        :forest_rmse: Value of RMSE
        :forest_c: Correlation Coefficient
    """
    forest_mse = mean_squared_error(target, prediction)
    forest_rmse = np.sqrt(forest_mse)
    print("  - RMSE is: {}".format(forest_rmse)) 
    
    forest_c = np.corrcoef(np.transpose(target), np.transpose(prediction))
    print("  - Correlation Coefficient is: {}".format(forest_c[0][1])) 
    
    return forest_rmse, forest_c[0][1]
