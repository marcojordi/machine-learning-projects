# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import os
import sys
import traceback
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use("../../plot_style.mplstyle")
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Wed Oct 10 15:09:24 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################
   
if __name__ == "__main__":

    try:
        #create Data
        y = np.array([2,2.5,3,3.2,5,4,7,5,6.7,7,8,7.3,6,9,10])
        x = np.linspace(0,10,len(y)).reshape(-1,1)
        
        #Train Regression Model
        from sklearn.linear_model import LinearRegression
        reg = LinearRegression()
        reg.fit(x,y)
        
        #Read Regression coefficients
        a = reg.intercept_
        b = reg.coef_
        
        #create plot
        plt.figure()
        plt.scatter(x,y)
        plt.plot(x, a+b*x, color="darkred")
        plt.legend(["Data", "Regression"])
        plt.xlabel("x")
        plt.ylabel("y")
        plt.title("Linear Regression")
        
    except:
        traceback.print_exc()
