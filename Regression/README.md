# Projects with Regression tasks
This directory contains various Machine Learning Projects with regression tasks, written in Python 3.6.xx using the Machine Learning libraries [Scikit-Learn](https://github.com/scikit-learn/scikit-learn) and [Keras](https://github.com/keras-team/keras) (backend [TensorFlow](https://github.com/tensorflow/tensorflow)).

- Linear Regression example:

	Basic Example of linear Regression

- Random Forest Regressor example:

	Example with three different input signal and one output signal
  

