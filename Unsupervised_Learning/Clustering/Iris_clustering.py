# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import traceback
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use("../../plot_style.mplstyle")
#scikit-learn
from sklearn import datasets
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import linkage, dendrogram
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Wed Nov 28 13:36:07 2018

@author: Marco Jordi

Example of towards data science
https://towardsdatascience.com/unsupervised-learning-with-python-173c51dc7f03
"""
###############################################################################
###############################################################################
   
if __name__ == "__main__":

    try:
        
        # Loading dataset
        iris_df = datasets.load_iris()
        
        #Scatter plot of labeled data
        x_axis = iris_df.data[:, 0]  # Sepal Length
        y_axis = iris_df.data[:, 2]  # Sepal Width
        
        fig = plt.figure()
        ax = fig.add_subplot(121)
        scat = ax.scatter(x_axis, y_axis, c=iris_df.target)
        ax.set_xlabel("Sepal Length")
        ax.set_ylabel("Sepal Width")
        ax.set_title("Original data")
        
        #######################################################################
        #K-means clustering
        model = KMeans(n_clusters=3)    #Initialize model
        model.fit(iris_df.data)         #Train model
        
        # Predicitng a  new single input
        predicted_label = model.predict([[7.2, 3.5, 0.8, 1.6]])
        
        # Prediction on the entire data
        all_predictions = model.predict(iris_df.data)
        
        #plot predicted data
        bx = fig.add_subplot(122)
        bx.scatter(x_axis, y_axis, c=all_predictions)
        bx.set_xlabel("Sepal Length")
        bx.set_ylabel("Sepal Width")
        bx.set_title("K-means clustering")
        
        #######################################################################
        #Hierarchical Clustering
        mergings = linkage(iris_df.data, method='complete')
        
        plt.figure()
        dendrogram(mergings, leaf_rotation=90, leaf_font_size=6)
        plt.title('Iris Hierarchical Clustering Dendrogram')
        plt.xlabel('Species')
        plt.ylabel('distance')
        
        
    
    except:
        traceback.print_exc()
