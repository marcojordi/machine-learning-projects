# Machine Learning Projects
This repository contains various Machine Learning Projects, written in Python 3.6.xx using the Machine Learning libraries [Scikit-Learn](https://github.com/scikit-learn/scikit-learn) and [Keras](https://github.com/keras-team/keras) (backend [TensorFlow](https://github.com/tensorflow/tensorflow)). The projects are divided into directories according to the different types of Machine Learning.

- Classification
- Regression
- Time Series Forecasting (RNN)
- Unsupervised Learning
- Reinforcement Learning

