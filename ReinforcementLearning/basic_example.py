# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import traceback
import gym
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Thu Nov 29 13:47:04 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################
   
if __name__ == "__main__":

    try:
        
        #Basic example of CartPole problem
        env = gym.make('CartPole-v0')
        
        #Classic agent-environment loop
        for i_episode in range(20):
            observation = env.reset()
            
            for t in range(100):
                env.render()
                print(observation)
                action = env.action_space.sample()
                observation, reward, done, info = env.step(action)
                
                #Check if its time to reset
                if done:
                    print("Episode finished after {} timesteps".format(t+1))
                    break
                
    except:
        traceback.print_exc()
