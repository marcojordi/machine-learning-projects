# Projects for Time Series Forecasting
This directory contains various Machine Learning Projects to predict time series, written in Python 3.6.xx using the Machine Learning libraries [Scikit-Learn](https://github.com/scikit-learn/scikit-learn) and [Keras](https://github.com/keras-team/keras) (backend [TensorFlow](https://github.com/tensorflow/tensorflow)).

- LSTM_basic_exampple:

	Basic Example to predict a noisy sinus signal with LSTM Network
  

