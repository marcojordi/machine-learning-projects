# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
import math
import traceback
import numpy as np
import matplotlib.pyplot as plt
plt.style.use("../../plot_style.mplstyle")
#keras
from keras.models import Sequential
from keras.layers import Dense, LSTM
#scikit-learn
from sklearn.metrics import mean_squared_error
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']
"""
Created on Thu Feb 15 15:04:33 2018

Descripton: 
    
"""
###############################################################################
###############################################################################

def create_dummydata(length):
    """Create dummy data for ML code"""
    a = np.linspace(0,100,length)
    b = np.random.random((length,1))/10
    c = np.array([np.sin(a)+b[:,0]])
    dataset = np.transpose(c)
    return dataset


def create_dataset(dataset, look_back=5):
    """Convert an array of values into a dataset matrix """
    dataX, dataY = [], []
    for i in range(len(dataset)-look_back-1):
        a = dataset[i:(i+look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
    return np.array(dataX), np.array(dataY)


def plot_predictions(dataset, trainPredict, testPredict):
    """Plots the training and test data predictions
    """
    # shift train predictions for plotting
    trainPredictPlot = np.empty_like(dataset)
    trainPredictPlot[:, :] = np.nan
    trainPredictPlot[look_back:len(trainPredict)+look_back, :] = trainPredict
    # shift test predictions for plotting
    testPredictPlot = np.empty_like(dataset)
    testPredictPlot[:, :] = np.nan
    testPredictPlot[len(trainPredict)+(look_back*2)+1:len(dataset)-1, :] = testPredict
    # plot baseline and predictions
    plt.plot(dataset, label="original")
    plt.plot(trainPredictPlot, label="training")
    plt.plot(testPredictPlot, label="testing")
    plt.title("Time Series Forecasting with LSTM Network")
    plt.xlabel("Samples")
    plt.ylabel("Amplitude")
    plt.ylim([-1.6,1.6])
    plt.legend(loc=1)
    
def create_RNN():
    """Creates the model of the reccurent neural network (RNN)
    """
    #Initialize model
    model = Sequential()
    
    #Input Layer
    model.add(LSTM(8, input_shape=(1, look_back), activation="tanh"))
    
    #Output Layer
    model.add(Dense(1, activation="linear"))
    
    #Compile model
    model.compile(loss="mean_squared_error", optimizer="nadam")
    model.summary()
    
    return model

###############################################################################

if __name__ == "__main__":

    try:
      
        #Generate dummy data (already normalized)      
        dataset = create_dummydata(1000)
        train = dataset[:800]
        test = dataset[800:]
        
        #Prepare data for LSTM 
        #(X is the input, last 5 samples and Y is the target, next sample)
        look_back = 5
        trainX, trainY = create_dataset(train, look_back)
        testX, testY = create_dataset(test, look_back)
        
        #Reshape Data for LSTM input
        trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
        testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))
        
        #Creating RNN with LSTM
        model = create_RNN()
        
        #Train RNN
        model.fit(trainX, trainY, epochs=50, batch_size=10, 
                  validation_split=0.15, verbose=2, shuffle=False)
        
        # make predictions
        trainPredict = model.predict(trainX)
        testPredict = model.predict(testX)
        
        # calculate root mean squared error
        trainScore = math.sqrt(mean_squared_error(trainY, trainPredict[:,0]))
        print("\nTrain Score RMSE: %.2f" % (trainScore))
        testScore = math.sqrt(mean_squared_error(testY, testPredict[:,0]))
        print("Test Score RMSE: %.2f" % (testScore))
        
        #Plotting data
        plot_predictions(dataset, trainPredict, testPredict)
        
    except:
        traceback.print_exc()



